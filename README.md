# ods-php

fork from https://sourceforge.net/projects/ods-php where last version was in 2008.
Thanks to Juan and Jose for create this simple library 

## Why ods-php is interresting?
There are other libraries as https://github.com/nuovo/spreadsheet-reader/blob/master/SpreadsheetReader_ODS.php which offer many more… and I only want OpenDocument Spreadsheet (ODS) management and working under old php 5 server.

## Why fork?
First test on my PC with PHP 7 return issue on constructor in ods class.
The _example.php_ original demonstrated only writing in spreadsheet and at first need, I only need to read.
Second issue, I have to read french files with accentuated characters. If there are no issue regarding casse (utf-8), sentence with accentuated character or any characters that require two bytes are cut in severals parts. Only the last part was returned.

## Usage
Just clone it and run  
**php example.php**  
or copy ods.php and example.php under web server and run <server_path>/example.php in your web browser  
For PHP5, use ods-php5.php instead of ods.php

## Main functions and methods
Read example.php to understand functions and methods usage

| function/method | description |
| --- | --- |
|newOds() |  function to create a new object
|parseOds() |  function to read file
|saveOds() |  function to save ods object in a file
|addCell() |  ods class method to add a cell with its value
|editCell() |  ods class method to change a cell value
|readCell() |  ods class method to read a cell value

