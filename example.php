<?php
/*

ods-php a library to read and write ods files from php.

This library has been forked from eyeOS project and licended under the LGPL3
terms available at: http://www.gnu.org/licenses/lgpl-3.0.txt (relicenced
with permission of the copyright holders)

*/
header('Content-Type: text/html; charset=utf-8');
$is_web=http_response_code()!==FALSE; // check if executed under web-server… or CLI
if (http_response_code()!==FALSE)
	$CR = "<br>";
else
	$CR = "\n";

include("ods.php"); //include the class and wrappers
$object = newOds(); //create a new ods file
$object->addCell(0,0,0,1,'float'); //add a cell to sheet 0, row 0, cell 0, with value 1 and type float
$object->addCell(0,0,1,'sentence with accentuated characters (caractères en Français)','string'); //add a cell to sheet 0, row 0, cell 1, with value as string
$object->addCell(0,1,0,1,'float'); //add a cell to sheet 0, row 1, cell 0, with value 1 and type float
$object->addCell(0,1,1,2.345,'float'); //add a cell to sheet 0, row 1, cell 1, with value 1 and type float
$object->addCell(0,2,0,"2020-02-24",'date'); //add a cell to sheet 0, row 2, cell 0, with date value 
saveOds($object,'/tmp/new.ods'); //save the object to a ods file

$object=parseOds('/tmp/new.ods'); //load the ods file
// read it
for ($i=0; $i<3; $i++) {
	for ($j=0; $j < 2; $j++) 
		echo $object->readCell(0,$i,$j) . ", ";
	echo $CR;
}
$object->editCell(0,0,0,25); //change the value for the cell in sheet 0, row 0, cell 0, to 25
echo $CR . "After change A1 (row 0, cell 0) cell value:" . $CR;
echo $object->readCell(0,0,0);
saveOds($object,'/tmp/new2.ods'); //save with other name
echo $CR;


?>
